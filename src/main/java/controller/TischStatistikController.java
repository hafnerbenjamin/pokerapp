package controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import objecte.Connector;
import objecte.Statistik;
import objecte.Tisch;

import java.util.List;

/**
 * Created by b7hafnb on 06.09.2016.
 */
public class TischStatistikController {
    @FXML
    private TableView<Tisch> tischTableViewTS;
    @FXML
    private TableColumn<Tisch, String> tischnameColumnTS;
    @FXML
    private TableColumn<Tisch, String> strasseColumnTS;
    @FXML
    private TableColumn<Tisch, String> ortColumnTS;
    @FXML
    private Button refreshTable;

    @FXML
    private Button max;
    @FXML
    private Button avg;
    @FXML
    private Button min;

    @FXML
    private BarChart<String, Integer> barChartTisch;

    Tisch aktTisch;

    public void initialize() {
        loadAllTischen();

        tischTableViewTS.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> tischAnwaehlen(newValue));

        refreshTable.setOnAction(event1 -> {
            Connector.connect();
            loadAllTischen();
        });

        max.setOnAction(event -> {
            if (istTischAngewaeht()) {
               getStat(Connector.getStatistikTischMax());
            }
        });

        avg.setOnAction(event -> {
            if (istTischAngewaeht()) {
                getStat(Connector.getStatistikTischAvg());
            }
        });

        min.setOnAction(event -> {
            if (istTischAngewaeht()) {
                getStat(Connector.getStatistikTischMin());
            }
        });
    }

    private void getStat(List<Statistik> statistik){
        for (Statistik s : statistik) {
            if(aktTisch.getName().equals(s.getTisch().getName())) {
                if(s.getVerlust() == 0) {
                    statNichtVorhanden();
                }else {
                    showStat(s.getVerlust(), s.getGewinn());
                }
            }
        }
    }

    private void statNichtVorhanden (){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText("Keine Daten vorhanden!");
        alert.setContentText("Am diesem Tisch wurde noch nie gespielt.");
        alert.showAndWait();
    }

    private void showStat (int verluste, int gewinne){
        barChartTisch.getData().clear();
        XYChart.Series<String, Integer> verlust = new XYChart.Series<>();
        verlust.getData().add(new XYChart.Data<>("", - verluste));
        verlust.setName("Verlust");
        barChartTisch.getData().add(verlust);

        XYChart.Series<String, Integer> gewinn = new XYChart.Series<>();
        gewinn.getData().add(new XYChart.Data<>("", gewinne));
        gewinn.setName("Gewinn");
        barChartTisch.getData().add(gewinn);
    }

    private void tischAnwaehlen(Tisch t) {
        barChartTisch.getData().clear();
        aktTisch = t;
    }

    private boolean istTischAngewaeht(){
        if(aktTisch == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Es muss ein Tisch angewaeht sein!");
            alert.setContentText("Bitte waehle einen Tisch.");
            alert.showAndWait();
            return false;
        }else {
            return true;
        }
    }


    private void loadAllTischen() {
        tischTableViewTS.getItems().clear();
        tischTableViewTS.getItems().addAll(Connector.getTische());
        createTischColumn();
    }

    private void createTischColumn() {
        tischnameColumnTS.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getName()));
        strasseColumnTS.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getAdresse().getStrasse() + " " + tisch.getValue().getAdresse().getHausnr()));
        ortColumnTS.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getAdresse().getPlz() + " " + tisch.getValue().getAdresse().getOrt()));
    }
}
