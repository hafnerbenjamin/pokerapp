package controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import objecte.Connector;
import objecte.Person;
import objecte.Statistik;

import java.util.List;


/**
 * Created by b7hafnb on 06.09.2016.
 */
public class PersonenStatistikController {
    @FXML
    private TableView<Person> personTableViewPS;
    @FXML
    private TableColumn<Person, String> benutzernameColumnPS;
    @FXML
    private TableColumn<Person, String> vornameColumnPS;
    @FXML
    private TableColumn<Person, String> nachnameColumnPS;
    @FXML
    private Button refreshPerson;

    @FXML
    private Button komplett;
    @FXML
    private Button jahr;
    @FXML
    private Button a30tag;
    @FXML
    private Button a7tag;

    @FXML
    private BarChart<String, Integer> barChart;
    Person aktPerson;

    public void initialize() {
        loadAllPersonen();

        personTableViewPS.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> personAnwaelen(newValue));

        refreshPerson.setOnAction(event1 -> {
            Connector.connect();
            loadAllPersonen();
        });


        komplett.setOnAction(event -> {
            if (istPersonAngewaeht()) {
                getStat(Connector.getStatistikAll());
            }
        });

        jahr.setOnAction(event -> {
            if (istPersonAngewaeht()) {
                getStat(Connector.getStatistikJahr());
            }
        });

        a30tag.setOnAction(event -> {
            if (istPersonAngewaeht()) {
                getStat(Connector.getStatistik30d());
            }
        });

        a7tag.setOnAction(event -> {
            if (istPersonAngewaeht()) {
                getStat(Connector.getStatistik7d());
            }
        });
    }

    private void getStat(List<Statistik> statistik){
        for (Statistik s : statistik){
            if(aktPerson.getBenutzername().equals(s.getPerson().getBenutzername())) {
                if(s.getVerlust() == 0) {
                    statNichtVorhanden();
                }else {
                    showStat(s.getVerlust(), s.getGewinn());
                }
            }
        }
    }

    private void statNichtVorhanden (){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText("Keine Daten vorhanden!");
        alert.setContentText("Diese Person hat noch nie gespielt.");
        alert.showAndWait();
    }

    private void showStat (int verluste, int gewinne){
        XYChart.Series<String, Integer> verlust = new XYChart.Series<>();
        verlust.getData().add(new XYChart.Data<>("", - verluste));
        verlust.setName("Verlust");
        barChart.getData().clear();
        barChart.getData().add(verlust);

        XYChart.Series<String, Integer> gewinn = new XYChart.Series<>();
        gewinn.getData().add(new XYChart.Data<>("", gewinne));
        gewinn.setName("Gewinn");
        barChart.getData().add(gewinn);

        XYChart.Series<String, Integer> balace = new XYChart.Series<>();
        balace.getData().add(new XYChart.Data<>("", gewinne - verluste));
        balace.setName("Balace");
        barChart.getData().add(balace);
    }

    private void personAnwaelen(Person p) {
        barChart.getData().clear();
        aktPerson = p;
    }

    private boolean istPersonAngewaeht (){
        if(aktPerson == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Es muss eine Person angewaeht sein!");
            alert.setContentText("Bitte waehle eine Person.");
            alert.showAndWait();
            return false;
        }else {
            return true;
        }
    }


    private void loadAllPersonen() {
        personTableViewPS.getItems().clear();
        personTableViewPS.getItems().addAll(Connector.getPersonen());
        createPersonColumn();
    }


    private void createPersonColumn() {
        benutzernameColumnPS.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getBenutzername()));
        vornameColumnPS.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getVorname()));
        nachnameColumnPS.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getNachname()));
    }
}
