package controller;

import dao.tisch.MySqlTischDAO;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import objecte.Adresse;
import objecte.Connector;
import objecte.Person;
import objecte.Tisch;

import java.util.Optional;

/**
 * Created by b7hafnb on 02.09.2016.
 */
public class TischController {
    @FXML
    private TableView<Tisch> tischTableView1;
    @FXML
    private TableColumn<Tisch, String> tischnameColumn1;
    @FXML
    private TableColumn<Tisch, String> strasseColumn1;
    @FXML
    private TableColumn<Tisch, String> ortColumn1;
    @FXML
    private TextField tischName;
    @FXML
    private TextField tischStrasse;
    @FXML
    private TextField tischHausNr;
    @FXML
    private TextField tischPlz;
    @FXML
    private TextField tischOrt;
    @FXML
    private Button tischNeu;
    @FXML
    private Button tischSpeichern;

    MySqlTischDAO tischDb = new MySqlTischDAO();
    Tisch aktTisch;


    public void initialize() {
        loadAllTischen();

        tischTableView1.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> tischAnwaelen(newValue));

        tischNeu.setOnAction(event -> {
            tischName.setText("");
            tischStrasse.setText("");
            tischHausNr.setText("");
            tischPlz.setText("");
            tischOrt.setText("");
        });

        tischSpeichern.setOnAction(event -> {
            Tisch t = new Tisch(tischName.getText());
            t.setAdresse(new Adresse(tischStrasse.getText(), tischHausNr.getText(), tischPlz.getText(), tischOrt.getText()));
            if (validierung(t)) {
                if (nameVergeben(t)){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Frage Dialog");
                    alert.setHeaderText("Dieser Tisch existiert schon!");
                    alert.setContentText("Willst du diesen Tisch wirklich anpassen?");
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK) {
                        tischDb.updateTisch(t);
                    }
                }else {
                    tischDb.addTisch(t);
                }
                Connector.connect();
                loadAllTischen();
            }

        });
    }

    private boolean nameVergeben(Tisch tisch){
        for (Tisch t: Connector.getTische()){
            if(tisch.getName().equals(t.getName())){
                return true;
            }
        }
        return false;
    }

    private boolean validierung (Tisch tisch){
        if(tisch.getName().equals("") || tisch.getAdresse().getStrasse().equals("") || tisch.getAdresse().getHausnr().equals("") || tisch.getAdresse().getPlz().equals("") || tisch.getAdresse().getOrt().equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Es muessen alle Felder ausgefuellt werden!");
            alert.setContentText("Stelle sicher das ueberall Daten sind.");
            alert.showAndWait();
            return false;
        }else {
            return true;
        }
    }

    private void tischAnwaelen(Tisch t) {
        aktTisch = t;
        tischName.setText(t.getName());
        tischStrasse.setText(t.getAdresse().getStrasse());
        tischHausNr.setText(t.getAdresse().getHausnr());
        tischPlz.setText(t.getAdresse().getPlz());
        tischOrt.setText(t.getAdresse().getOrt());
    }

    private void loadAllTischen() {
        tischTableView1.getItems().clear();
        tischTableView1.getItems().addAll(Connector.getTische());
        createTischColumn();
    }

    private void createTischColumn() {
        tischnameColumn1.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getName()));
        strasseColumn1.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getAdresse().getStrasse() + " " + tisch.getValue().getAdresse().getHausnr()));
        ortColumn1.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getAdresse().getPlz() + " " + tisch.getValue().getAdresse().getOrt()));
    }

}