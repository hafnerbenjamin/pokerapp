package controller;

import dao.statstik.MySqlStatstikDAO;
import dao.tisch.MySqlTischDAO;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import objecte.Connector;
import objecte.Person;
import objecte.Tisch;

import java.util.Optional;

/**
 * Created by b7hafnb on 01.09.2016.
 */
public class PlayController {

    @FXML
    private TableView<Person> personTableView;
    @FXML
    private TableColumn<Person, String> benutzernameColumn;
    @FXML
    private TableColumn<Person, String> vornameColumn;
    @FXML
    private TableColumn<Person, String> nachnameColumn;

    @FXML
    private TableView<Tisch> tischTableView;
    @FXML
    private TableColumn<Tisch, String> tischnameColumn;
    @FXML
    private TableColumn<Tisch, String> tischStrasseColumn;
    @FXML
    private TableColumn<Tisch, String> tischOrtColumn;

    @FXML
    private Button refreshTable;
    @FXML
    private Button refreshPerson;

    @FXML
    private Button gewinnSpeichern;
    @FXML
    private Button verlustSpeichern;

    @FXML
    private TextField gewinn;
    @FXML
    private TextField verlust;
    @FXML
    private CheckBox dealer;

    MySqlTischDAO tischDb = new MySqlTischDAO();
    MySqlStatstikDAO statstikDb = new MySqlStatstikDAO();

    Person aktPerson;
    Tisch aktTisch;

    public void initialize() {
        loadAllPersonen();
        loadAllTables();

        verlust.setText("10");
        gewinn.setText("0");

        tischTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> tischAnwaelen(newValue));
        personTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> personAnwaelen(newValue));

        refreshTable.setOnAction(event1 -> {
            Connector.connect();
            loadAllTables();
        });

        refreshPerson.setOnAction(event1 -> {
            Connector.connect();
            loadAllPersonen();
        });

        gewinnSpeichern.setOnAction(event -> {
            if (validierung(aktPerson, aktTisch, gewinnInFranken())) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Bestaetigungs Dialog");
                alert.setHeaderText("Frage");
                alert.setContentText("Willst du " + aktPerson.getBenutzername() + " " + gewinnInFranken() + ".- Franken am Tisch " + aktTisch.getName() + " ausbezahlen");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK){
                    statstikDb.addGewinn(aktPerson, aktTisch, gewinnInFranken());
                }
            }
        });

        verlustSpeichern.setOnAction(event -> {
            if (validierung(aktPerson, aktTisch, verlustInFranken())) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Bestaetigungs Dialog");
                alert.setHeaderText("Frage");
                alert.setContentText("Willst du " + aktPerson.getBenutzername() + " mit " + verlustInFranken() + ".- Franken am Tisch " + aktTisch.getName() + " einkaufen");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    statstikDb.addVerlust(aktPerson, aktTisch, verlustInFranken());
                }
            }
        });

        dealer.setOnAction((event) -> {
            if (dealer.isSelected()) {
                if (aktPerson != null && aktTisch != null) {
                    if(aktTisch.getDealer() == null) {
                        tischDb.addDealer(aktTisch, aktPerson);
                        Connector.connect();
                        loadAllTables();
                    }else {
                        dealer.setSelected(false);
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error Dialog");
                        alert.setHeaderText("Nur 1 Dealer!");
                        alert.setContentText("Es darf nur 1 Dealer pro Tisch geben.");
                        alert.showAndWait();
                    }
                } else {
                    dealer.setSelected(false);
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialog");
                    alert.setHeaderText("Tisch und Person anwaehlen!");
                    alert.setContentText("Es muss ein Tisch und eine Person ausgewaehlt werden.");
                    alert.showAndWait();
                }
            } else {
                tischDb.deleteDealer(aktTisch, aktPerson);
                Connector.connect();
                loadAllTables();
            }

        });
    }


    private void tischAnwaelen(Tisch t){
        aktTisch = t;
        dealerPruefen(aktTisch, aktPerson);
    }

    private void personAnwaelen(Person p){
        aktPerson = p;
        dealerPruefen(aktTisch, aktPerson);
    }

    private void dealerPruefen(Tisch tisch, Person person) {
        try{
            if (tisch.getDealer().getBenutzername().equals(person.getBenutzername())) {
                dealer.setSelected(true);
            } else {
                dealer.setSelected(false);
            }
        }catch (Exception e){
            dealer.setSelected(false);
        }
    }

    private boolean validierung(Person p, Tisch t, Integer i) {
        if(p == null || t == null || (i<1)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Tisch, Person, Betrag anwaehlen!");
            alert.setContentText("Es muss ein Tisch und eine Person und ein Betrag groesser 0 ausgewaehlt werden.");
            alert.showAndWait();
            return false;
        }
        return true;
    }


    private Integer gewinnInFranken(){
        try {
           return Integer.parseInt(gewinn.getText());
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Im Feld Gewinn ist keine Ganzzahl!");
            alert.setContentText("Es muss eine positive Zahl ohne komma eingegenben werden.");
            alert.showAndWait();
            return null;
        }
    }
    private Integer verlustInFranken(){
        try {
            return Integer.parseInt(verlust.getText());
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Im Feld Einkaufen ist keine Ganzzahl!");
            alert.setContentText("Es muss eine positive Zahl ohne komma eingegenben werden.");
            alert.showAndWait();
            return 0;
        }
    }

    private void loadAllPersonen() {
        personTableView.getItems().clear();
        personTableView.getItems().addAll(Connector.getPersonen());
        createPersonColumn();
    }

    private void createPersonColumn() {
        benutzernameColumn.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getBenutzername()));
        vornameColumn.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getVorname()));
        nachnameColumn.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getNachname()));
    }

    private void loadAllTables() {
        tischTableView.getItems().clear();
        tischTableView.getItems().addAll(Connector.getTische());
        createTableColumn();
    }
    private void createTableColumn() {
        tischnameColumn.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getName()));
        tischStrasseColumn.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getAdresse().getStrasse() + " " + tisch.getValue().getAdresse().getHausnr()));
        tischOrtColumn.setCellValueFactory(tisch -> new SimpleStringProperty(tisch.getValue().getAdresse().getPlz() + " " + tisch.getValue().getAdresse().getOrt()));
    }
}
