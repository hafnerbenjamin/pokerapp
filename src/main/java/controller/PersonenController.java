package controller;

import dao.person.MySqlPersonDAO;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import objecte.Adresse;
import objecte.Connector;
import objecte.Person;

import java.util.Optional;


/**
 * Created by b7hafnb on 02.09.2016.
 */
public class PersonenController {
    @FXML
    private TableView<Person> personTableViewP;
    @FXML
    private TableColumn<Person, String> benutzernameColumnP;
    @FXML
    private TableColumn<Person, String> vornameColumnP;
    @FXML
    private TableColumn<Person, String> nachnameColumnP;
    @FXML
    private TextField benutzername;
    @FXML
    private PasswordField passwort;
    @FXML
    private TextField vorname;
    @FXML
    private TextField nachname;
    @FXML
    private TextField strasse;
    @FXML
    private TextField hausNr;
    @FXML
    private TextField plz;
    @FXML
    private TextField ort;
    @FXML
    private Button neu;
    @FXML
    private Button speichern;

    MySqlPersonDAO personDb = new MySqlPersonDAO();
    Person aktPerson;


    public void initialize() {
        loadAllPersonen();

        personTableViewP.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> personAnwaelen(newValue));

        neu.setOnAction(event -> {
            benutzername.setText("");
            passwort.setText("");
            vorname.setText("");
            nachname.setText("");
            strasse.setText("");
            hausNr.setText("");
            plz.setText("");
            ort.setText("");
        });

        speichern.setOnAction(event -> {
            Person p = new Person(benutzername.getText(), passwort.getText(), vorname.getText(), nachname.getText());
            p.setAdresse(new Adresse(strasse.getText(), hausNr.getText(), plz.getText(), ort.getText()));
            if(validierung(p)){
                if (benutzernameVergeben(p)){
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Frage Dialog");
                    alert.setHeaderText("Der Benutzername existiert schon!");
                    alert.setContentText("Willst du diesen Benutzer wirklich anpassen?");
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK) {
                        personDb.updatePerson(p);
                    }
                }else {
                    personDb.addPerson(p);
                }
                Connector.connect();
                loadAllPersonen();
            }
        });

    }

    private boolean validierung (Person person){
        if(person.getBenutzername().equals("") || person.getVorname().equals("") || person.getNachname().equals("") || person.getPasswort().equals("") || person.getAdresse().getStrasse().equals("") || person.getAdresse().getHausnr().equals("") || person.getAdresse().getPlz().equals("") || person.getAdresse().getOrt().equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Es muessen alle Felder ausgefuellt werden!");
            alert.setContentText("Stelle sicher das ueberall Daten sind.");
            alert.showAndWait();
            return false;
        }else if(passwortNichIo(person)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Passwort zu kurz!");
            alert.setContentText("Das Passwort muss min. 6 Zeichen haben.");
            alert.showAndWait();
            return false;
        }else {
            return true;
        }
    }

    private boolean passwortNichIo(Person person) {
        return person.getPasswort().length() < 6;
    }

    private boolean benutzernameVergeben(Person person){
        for (Person p: Connector.getPersonen()){
            if(person.getBenutzername().equals(p.getBenutzername())){
                return true;
            }
        }
        return false;
    }
    private void personAnwaelen(Person p){
        try {
            aktPerson = p;
            benutzername.setText(p.getBenutzername());
            passwort.setText(p.getPasswort());
            vorname.setText(p.getVorname());
            nachname.setText(p.getNachname());
            strasse.setText(p.getAdresse().getStrasse());
            hausNr.setText(p.getAdresse().getHausnr());
            plz.setText(p.getAdresse().getPlz());
            ort.setText(p.getAdresse().getOrt());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void loadAllPersonen() {
        personTableViewP.getItems().clear();
        personTableViewP.getItems().addAll(Connector.getPersonen());
        createPersonColumn();
    }

    private void createPersonColumn() {
        benutzernameColumnP.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getBenutzername()));
        vornameColumnP.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getVorname()));
        nachnameColumnP.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getNachname()));
    }
}
