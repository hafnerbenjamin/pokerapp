package objecte;

/**
 * Created by b7hafnb on 23.08.2016.
 */
public class Adresse {

    String strasse;
    String hausnr;
    String plz;
    String ort;

    public Adresse(String strasse, String hausnr, String plz, String ort) {

        this.strasse = strasse;
        this.hausnr = hausnr;
        this.plz = plz;
        this.ort = ort;
    }

    @Override
    public String toString() {
        return " strasse= '" + strasse + ' ' + hausnr + '\'' +
                ", ort= '" + plz + ort + '\'' +
                '}';
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnr() {
        return hausnr;
    }

    public void setHausnr(String hausnr) {
        this.hausnr = hausnr;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }
}
