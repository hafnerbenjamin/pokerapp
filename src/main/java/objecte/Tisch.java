package objecte;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by b7hafnb on 23.08.2016.
 */
public class Tisch {
    Integer dealerId;
    Integer tischId;
    String name;
    Adresse adresse;
    Person dealer;
    List<Person> players = new ArrayList<>();

    public Tisch(Integer tischId, String name, Integer dealer_id) {
        this.tischId = tischId;
        this.name = name;
        this.dealerId = dealer_id;
    }

    public Tisch(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tisch{" +
                "name= '" + name + '\'' +
                '}';
    }

    public Integer getDealerId(){
        return dealerId;
    }

    public Person getDealer() {
        return dealer;
    }

    public void setDealer(Person dealer) {
        this.dealer = dealer;
    }

    public List<Person> getPlayers() {
        return players;
    }

    public void setPlayers(List<Person> players) {
        this.players = players;
    }

    public void addPlayer(Person player) {
        players.add(player);
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public Integer getTischId() {
        return tischId;
    }

    public void setTischId(Integer tischId) {
        this.tischId = tischId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }





}
