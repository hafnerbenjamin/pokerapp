package objecte;

import dao.person.MySqlPersonDAO;
import dao.statstik.MySqlStatstikDAO;
import dao.tisch.MySqlTischDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by b7hafnb on 02.09.2016.
 */
public class Connector {

    private static List<Tisch> tische = new ArrayList<>();
    private static List<Person> personen = new ArrayList<>();
    private static List<Statistik> statistikAll = new ArrayList<>();
    private static List<Statistik> statistikJahr = new ArrayList<>();
    private static List<Statistik> statistik30d = new ArrayList<>();
    private static List<Statistik> statistik7d = new ArrayList<>();
    private static List<Statistik> statistikTischMax = new ArrayList<>();

    private static List<Statistik> statistikTischAvg = new ArrayList<>();
    private static List<Statistik> statistikTischMin = new ArrayList<>();

    private static MySqlTischDAO tischDb = new MySqlTischDAO();
    private static MySqlPersonDAO personDb = new MySqlPersonDAO();
    private static MySqlStatstikDAO statstikDb = new MySqlStatstikDAO();

    public static void connect(){
        tische.clear();
        personen.clear();

        statistikAll.clear();
        statistikJahr.clear();
        statistik30d.clear();
        statistik7d.clear();
        statistikTischMax.clear();
        statistikTischAvg.clear();
        statistikTischMin.clear();

        tische.addAll(tischDb.getAllTische());
        personen.addAll(personDb.getAllPersons());

        statistikAll.addAll(statstikDb.getPersonStatistikAlles());
        statistikJahr.addAll(statstikDb.getPersonStatistikDiesesJahr());
        statistik30d.addAll(statstikDb.getPersonStatistikLetzte30Tage());
        statistik7d.addAll(statstikDb.getPersonStatistikLetze7Tage());
        statistikTischMax.addAll(statstikDb.getTischStatistikMax());
        statistikTischAvg.addAll(statstikDb.getTischStatistikAvg());
        statistikTischMin.addAll(statstikDb.getTischStatistikMin());

        connectPersonsToTables(tische, personen);
        connectStatistics(statistikAll, personen, tische);
        connectStatistics(statistikJahr, personen, tische);
        connectStatistics(statistik30d, personen, tische);
        connectStatistics(statistik7d, personen, tische);
        connectStatistics(statistikTischMax, personen, tische);
        connectStatistics(statistikTischAvg, personen, tische);
        connectStatistics(statistikTischMin, personen, tische);

    }

    public static List<Tisch> getTische (){
        return tische;
    }

    public static List<Person> getPersonen(){
        return personen;
    }

    public static List<Statistik> getStatistikAll(){
        return statistikAll;
    }
    public static List<Statistik> getStatistik7d() {
        return statistik7d;
    }

    public static List<Statistik> getStatistikJahr() {
        return statistikJahr;
    }

    public static List<Statistik> getStatistik30d() {
        return statistik30d;
    }

    public static List<Statistik> getStatistikTischMax() {
        return statistikTischMax;
    }

    public static List<Statistik> getStatistikTischAvg() {
        return statistikTischAvg;
    }

    public static List<Statistik> getStatistikTischMin() {
        return statistikTischMin;
    }

    public static void connectStatistics(List<Statistik> statistik, List<Person> personen, List<Tisch> tische){
        connectStatisticsToPerson(statistik, personen);
        connectStatisticsToTisch(statistik, tische);
    }

    public static void connectStatisticsToPerson(List<Statistik> statistik, List<Person> personen){
        for (Statistik s : statistik) {
            if(s.getPersonId() == null){
                continue;
            }
            for (Person p: personen){
                if(s.getPersonId().equals(p.getPersonId())){
                    s.addPerson(p);
                }
            }
        }
    }

    public static void connectStatisticsToTisch(List<Statistik> statistik, List<Tisch> tisch){
        for (Statistik s : statistik) {
            if(s.getTischId() == null){
                continue;
            }
            for (Tisch t: tisch){
                if(s.getTischId().equals(t.getTischId())){
                    s.addTisch(t);
                }
            }
        }
    }

    public static void connectPersonsToTables(List<Tisch> tische, List<Person> personen){
        for (Tisch t : tische) {
            connectPlayersToTable(t, personen);
            connectDealerToTable(t, personen);
        }
    }

    public static void connectPlayersToTable(Tisch tisch, List<Person> persons){
        for(Person p : persons){
            for (Integer tischId: p.getTischeIds()){
                if(tisch.getTischId().equals(tischId)){
                    tisch.addPlayer(p);
                }
            }
        }

    }

    public static void connectDealerToTable(Tisch tisch, List<Person> persons){
        for(Person p : persons){
            if(tisch.getDealerId().equals(p.getPersonId())){
                tisch.setDealer(p);
            }
        }
    }
}
