package objecte;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by b7hafnb on 23.08.2016.
 */
public class Person {

    Integer personId;
    String benutzername;
    String passwort;
    String vorname;
    String nachname;
    Adresse adresse;
    List<Integer> tischeId = new ArrayList<>();

    public Person(String benutzername, String passwort) {
        this.benutzername = benutzername;
        this.passwort = passwort;
    }

    public Person(Integer personId, String benutzername, String passwort, String vorname, String nachname) {
        this.personId = personId;
        this.benutzername = benutzername;
        this.passwort = passwort;
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public Person(String benutzername, String passwort, String vorname, String nachname) {
        this.benutzername = benutzername;
        this.passwort = passwort;
        this.vorname = vorname;
        this.nachname = nachname;
    }

    @Override
    public String toString() {
        return "Person{" +
                "benutzername= '" + benutzername + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return benutzername.equals(person.benutzername);
    }

    @Override
    public int hashCode() {
        return personId.hashCode();
    }

    public List<Integer> getTischeIds() {
        return tischeId;
    }

    public void setTischeIds(List<Integer> tischeIds) {
        this.tischeId = tischeId;
    }

    public void addTisch(Integer tischId) {
        tischeId.add(tischId);
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getBenutzername() {
        return benutzername;
    }

    public void setBenutzername(String benutzername) {
        this.benutzername = benutzername;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Adresse getAdresse(){
        return adresse;
    }

    public void setAdresse(Adresse adresse){
        this.adresse = adresse;
    }


}
