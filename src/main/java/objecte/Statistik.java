package objecte;

/**
 * Created by b7hafnb on 24.08.2016.
 */
public class Statistik {
    Integer gewinn;

    Integer verlust;

    Integer personId;
    Integer tischId;
    Person person;
    Tisch tisch;
    public Statistik(Integer personId, Integer tischId, Integer gewinn, Integer verlust) {
        this.gewinn = gewinn;
        this.verlust = verlust;
        this.personId = personId;
        this.tischId = tischId;
    }

    public Statistik(Integer id, Integer gewinn, Integer verlust, boolean istPerson) {
        this.gewinn = gewinn;
        this.verlust = verlust;
        if (istPerson) {
            this.personId = id;
        }else {
            this.tischId = id;
        }
    }

    @Override
    public String toString(){
        return verlust +" "+gewinn;
    }

    public Person getPerson(){
        return person;
    }

    public Integer getPersonId(){
        return personId;
    }

    public Integer getGewinn() {
        return gewinn;
    }

    public void setGewinn(Integer gewinn) {
        this.gewinn = gewinn;
    }

    public Integer getVerlust() {
        return verlust;
    }

    public void setVerlust(Integer verlust) {
        this.verlust = verlust;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public void setTischId(Integer tischId) {
        this.tischId = tischId;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Tisch getTisch() {
        return tisch;
    }

    public void setTisch(Tisch tisch) {
        this.tisch = tisch;
    }

    public void addPerson(Person person) {
        this.person = person;
    }

    public Integer getTischId() {
        return tischId;
    }

    public void addTisch(Tisch tisch) {
        this.tisch = tisch;
    }
}
