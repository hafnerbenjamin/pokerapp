import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * Created by b7hafnb on 22.08.2016.
 */
public class Main extends Application {
    private AnchorPane rootLayout;
    private Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        openAPP();
    }

    public void openAPP() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/gui/RootScene.fxml"));
            rootLayout = (AnchorPane) loader.load();

            Scene scene = new Scene(rootLayout);

            connectScenes("/gui/PlayScene.fxml", 0);
            connectScenes("/gui/PersonStatistikScene.fxml", 1);
            connectScenes("/gui/TischStatistikScene.fxml", 2);
            connectScenes("/gui/PersonScene.fxml", 3);
            connectScenes("/gui/TischScene.fxml", 4);

            primaryStage.setTitle("Poker Statistik App");
            primaryStage.getIcons().add(new Image("/pic/pokerLogo.png"));
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void connectScenes(String loc, int menue) throws IOException {
        Accordion accordion = (Accordion) rootLayout.getChildren().get(0);
        AnchorPane anchorPane;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource(loc));
        anchorPane = loader.load();
        accordion.getPanes().get(menue).setContent(anchorPane);
    }
}
