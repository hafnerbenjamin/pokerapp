package dao.statstik;

import dao.ConnectionManager;
import objecte.Person;
import objecte.Statistik;
import objecte.Tisch;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by b7hafnb on 24.08.2016.
 */
public class MySqlStatstikDAO implements StatistikDAO {

    @Override
    public List<Statistik> getPersonStatistikAlles() {
        List<Statistik> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "SELECT person_id, verlust, gewinn FROM stat_over_all");
             ResultSet rs = statement.executeQuery();) {
            buildStatistikListFromResultSetWithPerson(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public List<Statistik> getPersonStatistikLetze7Tage() {
        List<Statistik> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "SELECT person_id, verlust, gewinn FROM stat_7d");
             ResultSet rs = statement.executeQuery();) {
            buildStatistikListFromResultSetWithPerson(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public List<Statistik> getPersonStatistikLetzte30Tage() {
        List<Statistik> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "SELECT person_id, verlust, gewinn FROM stat_1m");
             ResultSet rs = statement.executeQuery();) {
            buildStatistikListFromResultSetWithPerson(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public List<Statistik> getPersonStatistikDiesesJahr() {
        List<Statistik> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "SELECT person_id, verlust, gewinn FROM stat_this_y");
             ResultSet rs = statement.executeQuery();) {
            buildStatistikListFromResultSetWithPerson(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }


    @Override
    public List<Statistik> getTischStatistikMax() {
        List<Statistik> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "SELECT tisch_id, verlust, gewinn FROM TABLE_stat_max");
             ResultSet rs = statement.executeQuery();) {
            buildStatistikListFromResultSetWithTable(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public List<Statistik> getTischStatistikAvg() {
        List<Statistik> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "SELECT tisch_id, verlust, gewinn FROM TABLE_stat_avg");
             ResultSet rs = statement.executeQuery();) {
            buildStatistikListFromResultSetWithTable(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    public List<Statistik> getTischStatistikMin() {
        List<Statistik> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "SELECT tisch_id, verlust, gewinn FROM TABLE_stat_min");
             ResultSet rs = statement.executeQuery();) {
            buildStatistikListFromResultSetWithTable(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }


    private void buildStatistikListFromResultSetWithPerson(List<Statistik> results, ResultSet rs) throws SQLException {
        while (rs.next()) {
            Integer personId = rs.getInt(1);
            Integer verlust = rs.getInt(2);
            Integer gewinn = rs.getInt(3);

            Statistik statistik = new Statistik(personId, gewinn, verlust, true);
            results.add(statistik);
        }
    }

    private void buildStatistikListFromResultSetWithTable(List<Statistik> results, ResultSet rs) throws SQLException {
        while (rs.next()) {
            Integer tischId = rs.getInt(1);
            Integer verlust = rs.getInt(2);
            Integer gewinn = rs.getInt(3);

            Statistik statistik = new Statistik(tischId, gewinn, verlust, false);
            results.add(statistik);
        }
    }

    @Override
    public void addVerlust(Person person, Tisch tisch, Integer franken) {
        try(Connection conn = ConnectionManager.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO verlust (tisch_id, person_id, franken)\n" +
                        "VALUES(?,?,?)")) {

            statement.setInt(1, tisch.getTischId());
            statement.setInt(2, person.getPersonId());
            statement.setInt(3, franken);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void addGewinn(Person person, Tisch tisch, Integer franken) {
        try(Connection conn = ConnectionManager.getConnection();
            PreparedStatement statement = conn.prepareStatement(
                    "INSERT INTO gewinn (tisch_id, person_id, franken)\n" +
                            "VALUES(?,?,?)")) {

            statement.setInt(1, tisch.getTischId());
            statement.setInt(2, person.getPersonId());
            statement.setInt(3, franken);
            statement.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
