package dao.statstik;

import objecte.Person;
import objecte.Statistik;
import objecte.Tisch;

import java.util.List;

/**
 * Created by b7hafnb on 24.08.2016.
 */
public interface StatistikDAO {

    public List<Statistik> getPersonStatistikAlles();
    public List<Statistik> getPersonStatistikLetze7Tage();
    public List<Statistik> getPersonStatistikLetzte30Tage();
    public List<Statistik> getPersonStatistikDiesesJahr();

    public List<Statistik> getTischStatistikMax();
    public List<Statistik> getTischStatistikAvg();
    public List<Statistik> getTischStatistikMin();

    public void addVerlust(Person person, Tisch tisch, Integer franken);
    public void addGewinn(Person person, Tisch tisch, Integer franken);
}
