package dao.person;

import objecte.Person;
import java.util.List;

/**
 * Created by b7hafnb on 23.08.2016.
 */
public interface PersonDAO {

        public List<Person> getAllPersons();

        public void addPerson(Person person);

        public void updatePerson(Person person);

}
