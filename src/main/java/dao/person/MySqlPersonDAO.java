package dao.person;

import dao.ConnectionManager;
import objecte.Adresse;
import objecte.Person;
import objecte.Tisch;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by b7hafnb on 23.08.2016.
 */
public class MySqlPersonDAO implements PersonDAO {
    @Override
    public List<Person> getAllPersons() {
        List<Person> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                    " SELECT person_id, benutzername, passwort, vorname, nachname, strasse, hausnr, plz, ort FROM ort\n"+
                    " JOIN adresse\n"+
                    " USING (ort_id)\n"+
                    " JOIN person\n"+
                    " USING(adr_id)");
             ResultSet rs = statement.executeQuery();) {
            buildPersonListFromResultSet(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    private void buildPersonListFromResultSet(List<Person> results, ResultSet rs) throws SQLException {
        Map<Integer, Person> PersonsById = new HashMap<>();
        while (rs.next()) {
            Integer personId = rs.getInt(1);
            String benutzername = rs.getString(2);
            String passwort = rs.getString(3);
            String vorname = rs.getString(4);
            String nachname = rs.getString(5);
            String strasse = rs.getString(6);
            String hausnr = rs.getString(7);
            String plz = rs.getString(8);
            String ort = rs.getString(9);

            Person person = PersonsById.get(personId);
            if (person == null) {
                person = new Person(personId, benutzername, passwort, vorname, nachname);
                PersonsById.put(personId, person);
            }
            person.setAdresse(new Adresse (strasse, hausnr, plz, ort));
        }
        results.addAll(PersonsById.values());
    }
    
    @Override
    public void addPerson(Person person){
        insertPerson(addAdresse(addOrt(person.getAdresse()), person.getAdresse()), person);
    }

    private int addOrt(Adresse a){
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "SELECT ort_id FROM ort\n" +
                             "WHERE plz = (?)");) {
            stat.setString(1, a.getPlz());
            ResultSet resultSet = stat.executeQuery();
            if (resultSet.next() && resultSet.getInt(1) > 0) {
                return resultSet.getInt(1);
            } else {
                return ortNochNichtVorhanden(a);
            }
        }catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }

    private int ortNochNichtVorhanden(Adresse a)throws SQLException{
        Connection conn = ConnectionManager.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO ort (plz, ort)\n" +
                        "VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, a.getPlz());
        statement.setString(2, a.getOrt());
        statement.executeUpdate();

        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            return rs.getInt(1);
        } else {
            return 0;
        }
    }

    private int addAdresse(int ortId, Adresse a){
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "SELECT adr_id FROM adresse\n" +
                             "WHERE strasse = (?) && hausnr = (?)");) {
            stat.setString(1, a.getStrasse());
            stat.setString(2, a.getHausnr());

            ResultSet resultSet = stat.executeQuery();
            if (resultSet.next() && resultSet.getInt(1) > 0) {
                return resultSet.getInt(1);
            } else {
                return adresseNochNichtVorhanden(ortId, a);
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }

    private int adresseNochNichtVorhanden(int ortId, Adresse a) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO adresse (ort_id, strasse, hausnr)\n" +
                        "VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);

        statement.setInt(1, ortId);
        statement.setString(2, a.getStrasse());
        statement.setString(3, a.getHausnr());
        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            return rs.getInt(1);
        } else {
            return 0;
        }
    }


    public void insertPerson(int adressId, Person person) {
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "INSERT INTO person (adr_id, benutzername, passwort, vorname, nachname)\n" +
                             "VALUES(?,?,?,?,?)")) {

            statement.setInt(1, adressId);
            statement.setString(2, person.getBenutzername());
            statement.setString(3, person.getPasswort());
            statement.setString(4, person.getVorname());
            statement.setString(5, person.getNachname());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePerson(Person person) {
        updatingPerson(addAdresse(addOrt(person.getAdresse()), person.getAdresse()), person);
    }

    public void updatingPerson(int adressId, Person person) {
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "UPDATE person SET adr_id = (?), passwort = (?), vorname = (?), nachname = (?)\n" +
                             " WHERE benutzername = (?)")) {

            statement.setInt(1, adressId);
            statement.setString(2, person.getPasswort());
            statement.setString(3, person.getVorname());
            statement.setString(4, person.getNachname());
            statement.setString(5, person.getBenutzername());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
