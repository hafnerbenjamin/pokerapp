package dao.tisch;

import objecte.Person;
import objecte.Tisch;

import java.util.List;

/**
 * Created by b7hafnb on 23.08.2016.
 */
public interface TischDAO {

    public List<Tisch> getAllTische();

    public void addDealer(Tisch tisch, Person person);

    public void deleteDealer(Tisch tisch, Person person);

    public void addTisch(Tisch tisch);

    public void updateTisch(Tisch tisch);

}