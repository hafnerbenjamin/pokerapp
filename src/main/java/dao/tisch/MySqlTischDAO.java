package dao.tisch;

import dao.ConnectionManager;
import objecte.Adresse;
import objecte.Person;
import objecte.Tisch;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by b7hafnb on 23.08.2016.
 */
public class MySqlTischDAO implements TischDAO {
    @Override
    public List<Tisch> getAllTische() {
        List<Tisch> results = new ArrayList<>();
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "SELECT tisch_id, tisch_name, strasse, hausnr, plz, ort, person_id FROM ort\n" +
                     "JOIN adresse\n" +
                     "USING (ort_id)\n" +
                     "JOIN Tisch\n" +
                     "USING(adr_id)\n" +
                     "LEFT JOIN dealer\n" +
                     "USING (dealer_id)");

             ResultSet rs = statement.executeQuery();) {
            buildTischListFromResultSet(results, rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    private void buildTischListFromResultSet(List<Tisch> results, ResultSet rs) throws SQLException {
        Map<Integer, Tisch> TischeById = new HashMap<>();
        while (rs.next()) {
            Integer tischId = rs.getInt(1);
            String name = rs.getString(2);
            String strasse = rs.getString(3);
            String hausnr = rs.getString(4);
            String plz = rs.getString(5);
            String ort = rs.getString(6);
            Integer dealerId = rs.getInt(7);

            Tisch tisch = TischeById.get(tischId);
            if (tisch == null) {
                tisch = new Tisch(tischId, name, dealerId);
                TischeById.put(tischId, tisch);
            }
            tisch.setAdresse(new Adresse(strasse, hausnr, plz, ort));
        }
        results.addAll(TischeById.values());
    }


    @Override
    public void addDealer(Tisch tisch, Person person) {
        addDealerOnTable(tisch, getDealerId(person));
    }

    private int getDealerId(Person person){
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT dealer_id FROM dealer\n" +
                             "WHERE person_id = (?)");) {

            statement.setInt(1, person.getPersonId());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next() && resultSet.getInt(1) > 0){
                return resultSet.getInt(1);
            } else{
                try (Connection conn = ConnectionManager.getConnection();
                     PreparedStatement insertStatement = conn.prepareStatement(
                             "INSERT INTO dealer (person_id)\n" +
                                     "VALUES(?)", Statement.RETURN_GENERATED_KEYS)) {
                    insertStatement.setInt(1, person.getPersonId());
                    insertStatement.executeUpdate();
                    ResultSet rs = insertStatement.getGeneratedKeys();
                    if (rs.next()) {
                        return rs.getInt(1);
                    }else {
                        return 0;
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private void addDealerOnTable(Tisch tisch, Integer dealerId){
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "UPDATE tisch SET dealer_id = (?) WHERE tisch_id = (?)")) {
            statement.setInt(1, dealerId);
            statement.setInt(2, tisch.getTischId());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteDealer(Tisch tisch, Person person){
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "UPDATE tisch SET dealer_id = null WHERE tisch_id = (?)")) {
            statement.setInt(1, tisch.getTischId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void addTisch(Tisch tisch) {
        insertTisch(addAdresse(addOrt(tisch.getAdresse()), tisch.getAdresse()), tisch);
    }

    private int addOrt(Adresse a){
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "SELECT ort_id FROM ort\n" +
                             "WHERE plz = (?)");) {
            stat.setString(1, a.getPlz());
            ResultSet resultSet = stat.executeQuery();
            if (resultSet.next() && resultSet.getInt(1) > 0) {
                return resultSet.getInt(1);
            } else {
                return ortNochNichtVorhanden(a);
            }
        }catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }

    private int ortNochNichtVorhanden(Adresse a)throws SQLException{
        Connection conn = ConnectionManager.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO ort (plz, ort)\n" +
                        "VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, a.getPlz());
        statement.setString(2, a.getOrt());
        statement.executeUpdate();

        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            return rs.getInt(1);
        } else {
            return 0;
        }
    }

    private int addAdresse(int ortId, Adresse a){
        try (Connection connection = ConnectionManager.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "SELECT adr_id FROM adresse\n" +
                             "WHERE strasse = (?) && hausnr = (?)");) {
            stat.setString(1, a.getStrasse());
            stat.setString(2, a.getHausnr());

            ResultSet resultSet = stat.executeQuery();
            if (resultSet.next() && resultSet.getInt(1) > 0) {
                return resultSet.getInt(1);
            } else {
                return adresseNochNichtVorhanden(ortId, a);
            }
        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }

    private int adresseNochNichtVorhanden(int ortId, Adresse a) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        PreparedStatement statement = conn.prepareStatement(
                "INSERT INTO adresse (ort_id, strasse, hausnr)\n" +
                        "VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);

        statement.setInt(1, ortId);
        statement.setString(2, a.getStrasse());
        statement.setString(3, a.getHausnr());
        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            return rs.getInt(1);
        } else {
            return 0;
        }
    }

    public void insertTisch(int adressId, Tisch t) {
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "INSERT INTO tisch (adr_id, tisch_name)\n" +
                             "VALUES(?,?)")) {
            statement.setInt(1, adressId);
            statement.setString(2, t.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void updateTisch(Tisch tisch) {
        updatingTisch(addAdresse(addOrt(tisch.getAdresse()), tisch.getAdresse()), tisch);
    }

    public void updatingTisch(int adressId, Tisch tisch) {
        try (Connection conn = ConnectionManager.getConnection();
             PreparedStatement statement = conn.prepareStatement(
                     "UPDATE tisch SET adr_id = (?)\n" +
                             " WHERE tisch_name = (?)")) {

            statement.setInt(1, adressId);
            statement.setString(2, tisch.getName());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
